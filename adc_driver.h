#ifndef ADC_DRIVER
#define ADC_DRIVER
#define F_CPU 4915200
#include <avr/io.h>
#include <util/delay.h>


/*Struct containing the x and y positions given by percentage of maxumum angle.
0 = farthest left, 100 = farthest right.
*/
typedef struct {
    uint8_t x;
    uint8_t y;
} joystick_positions_t;

/*Struct for different direction values
*/
typedef enum direction{
    LEFT,
    RIGHT,
    UP,
    DOWN,
    NEUTRAL,
    ERROR
}direction_t;

void ADC_init(void);

uint8_t x_channel_adc_read(void);

uint8_t y_channel_adc_read(void);

joystick_positions_t get_joystick_positions(void);

direction_t get_direction(void);

uint8_t read_right_slider(void);

uint8_t read_left_slider(void);

char* direction_to_string(direction_t dir);

#endif