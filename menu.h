#ifndef LINKED_LIST
#define LINKED_LIST


struct Node { 
    char  *data; 
    struct Node* next; 
    struct Node* prev;
    struct Node* sub;
    struct Node* super;
}; 

void init_menu(void);

/*Prints the whole layer of the meny we are in currently, with the chose option highlighted.
*/
void print_menu(void);

void menu_up();
void menu_down();
struct Node* menu_pos();
void menu_sub();
void menu_super();
struct Node* get_head();
#endif