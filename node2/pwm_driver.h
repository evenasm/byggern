#ifndef PWM_DRIVER
#define PWM_DRIVER
#include <stdint.h>

void pwm_init();


/*
Updated the duty cycle with provided duty cycle.
@return 0 on success, -1 on failure (invalid dc)
*/
int pwm_update_dc(uint32_t dc);

void joystick_to_servo(uint8_t x);

#endif