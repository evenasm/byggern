#include "tc_driver.h"
#include "sam.h"

#define WAVEFORM_CONFIG 0x0009c002
#define PASSWD 0x54494D00
#define PASSWD_PIO 0x50494f00
#define PASSWD_PMC 0x504d4300

void tc_init(){
    //first we enable thje peripheral clock for TC0
    PMC->PMC_WPMR = PASSWD_PMC; //disable write protection
    PMC->PMC_SCER = 0x00000700; //enable all programmable clocks
    PMC->PMC_PCER0 |= PMC_PCER0_PID12; //enable clock on port B
    PMC->PMC_PCER0 |= PMC_PCER0_PID27; // enable clock on TC0

    //now we set this waveform out on pin 25 on port B
    PIOB->PIO_WPMR = PASSWD_PIO; //disable write protection
    //PIOB->PIO_OER |= PIO_PB25;
    PIOB->PIO_PDR |= PIO_PB15;
    PIOB->PIO_PER &= ~PIO_PB25; //disable PIO control on PB25 aka enable peripheral control
    PIOB->PIO_ABSR |=  PIO_PB25; //enable peripheral B (tioa0) on pin 25 port B
    
    
    REG_TC0_WPMR = PASSWD; //turn off write protection with password "TIM"
    REG_TC0_CCR0 = 0x00000000; //enable clock on tc0 channel 0
    REG_TC0_CCR0 = 0x00000001;
    REG_TC0_CMR0 = (TC_CMR_ACPA_SET | TC_CMR_ACPC_CLEAR | TC_CMR_WAVE | TC_CMR_WAVSEL_UP_RC | TC_CMR_TCCLKS_TIMER_CLOCK3);
    REG_TC0_RB0 = 0x8000;
    REG_TC0_RA0 = 0x6c66; //TIOA sets on Ra compare, which now happens at abt 18.5 ms (277450)
    REG_TC0_RC0 = 0x7530; // Set Rc aka period to 20 ms (number is 30 000)

    
}