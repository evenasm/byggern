#include <stdio.h>
#include <stdarg.h>
#include "uart_and_printf/uart.h"
#include "uart_and_printf/printf-stdarg.h"
#include "can_controller.h"
#include "tc_driver.h"
#include "sam.h"
#include "pwm_driver.h"
#define CAN_BR_INIT_VAlUE 0x00290165

void delay(int d){
    int a;
    for(int i = 1; i<d*3000000; i++){
            a = 1;
        }
}

int main()
{
    SystemInit();
    tc_init();
    pwm_init();

    WDT->WDT_MR = WDT_MR_WDDIS; //Disable Watchdog Timer

    configure_uart();
    can_init_def_tx_rx_mb(CAN_BR_INIT_VAlUE); //initialize can_br register with this 
    printf("Hello World\n\r");
    uint32_t  status = REG_TC0_SR0;
    printf("status = %x  \n\r", status);

    PMC->PMC_PCER0 |= (1 << 11); // enable peripheral clock
    PIOA->PIO_PER |= (3 << 19);
    PIOA->PIO_OER |= (3 << 19);
    //PIOA->PIO_ODR |= (3 << 19);

    /* uint32_t clokk = PMC->PMC_MCKR & (0b111 << 4);
    printf("Klokkestatus er: %x \n", clokk);
    uint32_t clfq = PMC->CKGR_MOR & (0b111 << 4);
    printf("Klokkehastighet er: %d \n", clfq); */
    PIOA->PIO_SODR = PIO_SODR_P19; 
    CAN_MESSAGE message;
    CAN_MESSAGE* msg= &message;
    CAN_MESSAGE back;
    int a;
    message.id = 1;
    message.data_length = 8;
    for(int i = 0; i < 8 ; i++){
        message.data[i] = 3*i;
    }
    //can_send(&message, 0);
    while (1)
    {
         for(int i = 0; i < 10; i++){
            delay(1);
            pwm_update_dc(2650 + 200*i);
        } 
        /* if(can_receive(msg, 0) == 0){
            pwm_update_dc(msg->data[0]);
        } */
        /* if(can_receive(msg, 0) == 0){
            printf("Got a message on mailbox 0:)\n\r");
            printf("ID = %x", msg->id);
            printf("Data length: %x\n\r", msg->data_length);
            for(int i = 0; i<8 ; i++){
                msg->data[i] = 3*msg->data[i];      
                printf("Data[%d] = %d\n \r",i,msg->data[i]);
                //back.data[i] = 3*msg->data[i];
            } 
            /* message.id = 0;
            message.data_length = 8;
            for(int i = 0; i < 8 ; i++){
                message.data[i] = i;
            } 
            if(can_send(msg, 0)!=1){
                printf("Sending\n\r");
            }
            
        
        }else if(can_receive(&msg, 1) == 0){
            printf("Got a message on mailbox 1\n \r");
        }  */
        /* for(int i = 1; i<2500000; i++){
            a = 1;
        }
        printf("One loop\n\r");
        message.id = 1;
        message.data_length = 8;
        for(int i = 0; i < 8 ; i++){
            message.data[i] = 2*i;
        }
        back.id = 2;
        back.data_length = 7; */
        /* if(can_send(&message, 0) == 1){
            printf("couldnt send\n\r");
        } */
              
    } 
    
}