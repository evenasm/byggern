#include "pwm_driver.h"
#include "sam.h"
#include <stdint.h>

#define PASSWRD_PWM 0x50574dfc
#define PASSWD_PMC 0x504d4300
#define PASSWD_PIO 0x50494f00

void pwm_init(){

    PMC->PMC_WPMR = PASSWD_PMC; //disable write protection
    PMC->PMC_SCER = 0x00000700; //enable all programmable clocks
    //PMC->PMC_PCER0 |= PMC_PCER0_PID12; //enable clock on port B
  //  PMC->PMC_PCER0 |= PMC_PCER0_PID11; //enable clock on port A
    PMC->PMC_PCER1 |= PMC_PCER1_PID36; // enable clock on PWM
    PMC->PMC_PCER0 |= PMC_PCER0_PID13; //enable peripheral clock on port C

    

    //now we set this waveform out on pin 25 on port B
    PIOC->PIO_WPMR = PASSWD_PIO; //disable write protection

    PIOC->PIO_OER |= PIO_PC18; // enable output on pin 8 port A
    PIOC->PIO_PER &= ~PIO_PC18; //disable PIO control on PA8 aka enable peripheral control
    PIOC->PIO_PDR |= PIO_PC18; //set pin 19 port C as output
    PIOC->PIO_ABSR |= PIO_ABSR_P18; //enable peripheral B (pwm high) on pin 

    /* PIOA->PIO_OER |= PIO_PB16;
    PIOA->PIO_PER &= ~PIO_PB16;
    printf("Peripheral control reg portb :%x \n\r", REG_PIOA_ABSR);
    PIOA->PIO_ABSR |= PIO_ABSR_P16;
    PIOB->PIO_PDR |= PIO_PDR_P16; */

//setup pwm signal
    PWM->PWM_WPCR = PASSWRD_PWM;
    REG_PWM_CLK = PWM_CLK_PREA(0) | PWM_CLK_DIVA(42);     // Set the PWM clock rate to 2MHz (84MHz/42)
    REG_PWM_CMR6 = (PWM_CMR_CALG | PWM_CMR_CPRE_CLKA );
    REG_PWM_CMR6 = (PWM_CMR_CPRE_MCK_DIV_32 | PWM_CMR_CPOL);
    REG_PWM_CPRD6 = 52500;
    REG_PWM_CDTY6 = 2650;
    REG_PWM_ENA = PWM_ENA_CHID6; 

    /* REG_PMC_PCER1 |= PMC_PCER1_PID36;                     // Enable PWM
    REG_PIOB_ABSR |= PIO_ABSR_P16;                        // Set PWM pin perhipheral type A or B, in this case B
    REG_PIOB_PDR |= PIO_PDR_P16;                          // Set PWM pin to an output
    REG_PWM_CLK = PWM_CLK_PREA(0) | PWM_CLK_DIVA(42);     // Set the PWM clock rate to 2MHz (84MHz/42)
    REG_PWM_CMR0 = PWM_CMR_CALG | PWM_CMR_CPRE_CLKA;      // Enable dual slope PWM and set the clock source as CLKA
    REG_PWM_CPRD0 = 30000;                                // Set the PWM frequency 2MHz/(2 * 20000) = 50Hz
    REG_PWM_CDTY0 = 2250;                                 // Set the PWM duty cycle to 1500 - centre the servo
    REG_PWM_ENA = PWM_ENA_CHID0; */                         // Enable the PWM channel 

    
}

int pwm_update_dc(uint32_t dc){
    if((2650 < dc) && (dc < 5300)){
        REG_PWM_CDTYUPD5 = dc;
        return 0;
    }
    else return -1;
    
}

void joystick_to_servo(uint8_t x){
    pwm_update_dc(2650 + 10*x);
}