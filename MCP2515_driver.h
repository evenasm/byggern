#ifndef MCP2515
#define MCP2515

char read_mcp(char address);

char write_mcp(char data, char address);

//Request to send
void RTS(unsigned char buffer_number);

char read_mcp_status();

void mcp_reset();

void mcp_bit_modify(char address, char mask, char data);

#endif