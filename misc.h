#ifndef MISC
#define MISC
#define F_CPU 4915200
#include <avr/io.h>
#include <stdlib.h>
#include <util/delay.h>
#include <stdio.h>
#include "adc_driver.h"
#include "OLED_driver.h"
#include "CAN_driver.h"


void test_latch(void);

void test_jtag(void);

void SRAM_test(void);

void test_slider(void);

void test_OLED_positioning();

void read_adc_output_test(void);

void bitwise_print(char data, char *p);

void print_can_message(can_message *rec);

#endif