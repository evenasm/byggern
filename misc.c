#include "misc.h"
/* Tests the
*/
void test_latch(void){
    double del = 250;
    DDRA |= (255); //enable input on pin 7 port A
    _delay_ms(del);
    DDRE |= ( 1 << PE1);
    PORTE &= ~(1 << PE1); 
    PORTE |= (1 << PE1); 
    PORTA |= (1 << PA0);
    _delay_ms(del);
    _delay_ms(del);
    PORTE &= ~(1 << PE1);
    PORTA &= 0;
    _delay_ms(1000.0);
}

void test_jtag(){
    double del = 250;
    int i = 1;
    DDRA |= (1 << PA0);
    while(i < 3){
        if(i % 2 == 0){
            PORTA &= ~(1 << PA0);
        }else{
            PORTA |= (1 << PA0);  
        }
        _delay_ms(del);
        i++;
    }
    
}

void SRAM_test(void)
    {
		volatile char *ext_ram = (char *) 0x1800; // Start address for the SRAM
        uint16_t ext_ram_size = 0x800;
        uint16_t write_errors = 0;
        uint16_t retrieval_errors = 0;
        printf("Starting SRAM test...\n");
        // rand() stores some internal state, so calling this function in a loop will
        // yield different seeds each time (unless srand() is called before this function)
        uint16_t seed = rand();
        // Write phase: Immediately check that the correct value was stored
        srand(seed);
        for (uint16_t i = 0; i < ext_ram_size; i++) {
            uint8_t some_value = i + 10;
            ext_ram[i] = some_value;
            uint8_t retreived_value = ext_ram[i];
            if (retreived_value != some_value) {
                printf("Write phase error: ext_ram[%4d] = %02X (should be %02X)\n", i, retreived_value, some_value);
                write_errors++;
            }
        }
        // Retrieval phase: Check that no values were changed during or after the write phase
        srand(seed);
        // reset the PRNG to the state it had before the write phase
        for (uint16_t i = 0; i < ext_ram_size; i++) {
            uint8_t some_value = i + 10;
            uint8_t retreived_value = ext_ram[i];
            if (retreived_value != some_value) {
                printf("Retrieval phase error: ext_ram[%4d] = %02X (should be %02X)\n", i, retreived_value, some_value);
                retrieval_errors++;
            }
        }
        printf("SRAM test completed with \n%4d errors in write phase and \n%4d errors in retrieval phase\n\n", write_errors, retrieval_errors);
}   

void test_slider(void){
    uint8_t right = read_right_slider();
    uint8_t left = read_left_slider();
    printf("\033\143");
    printf("Right, left: %d %d",right,left);
    _delay_ms(40);
}

void test_OLED_positioning(){
    char string[] = " Hei, verden";
    for(int i = 0; i < 8; i++){
        OLED_goto_line(i);
        OLED_goto_column(10*i);
        OLED_write_string(string,5,1);
        //OLED_write_string(string);
        _delay_ms(250);
    }
    OLED_home();
    OLED_clear_line(0);
    OLED_goto_line(0);
    OLED_write_string("test1",5,1);
    OLED_clear_line(5);
    OLED_pos((char) 5, (char) 60);
    OLED_write_string("test2",5,1);
 
}

void read_adc_output_test(void){
   uint8_t x_value = x_channel_adc_read();
    printf("X channel value: %d \n", x_value);
     uint8_t y_value = y_channel_adc_read();
    printf("Y channel value: %d \n", y_value); 
}

void bitwise_print(char data, char *p){
    char binary;
    for(int i = 0; i < 8; i++){
        binary = data &( 1 << i);
        if(binary){
            p[i] = 61;
        }else{
            p[i] = 60;
        }
    }
    p[8]='\0';
}

void print_can_message(can_message *rec){
    printf("ID =%x\n", rec->id);
    printf("Data length = %d \n", rec->data_length);
    for(int i = 0; i < rec->data_length && (i < 8); i++){
        printf("d[%d] = %d\n", i, (char) rec->data[i]);
    }
}