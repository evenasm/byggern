#include "OLED_driver.h"
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "fonts.h"
#include "adc_driver.h"

#define F_CPU 4915200
#include <util/delay.h>

volatile char *command_address = (char *) 0x1000;
volatile char *data_address = (char *) 0x1200;
 

void write_command_OLED(char command){
    //Sett pins høye iht kommando inn, kommandoområde 0x1000-0x11ff
    command_address[0]=command;
}

void OLED_clear(void){
    for(int i = 1; i < 1024 ; i++){
        write_d(0x00);
    }
}


void OLED_init(void){
    write_command_OLED(0xae); // display off
    write_command_OLED(0xa1); //segment remap
    write_command_OLED(0xda); //common pads hardware: alternative
    write_command_OLED(0x12);
    write_command_OLED(0xc8); //common output scan direction:com63~com0
    write_command_OLED(0xa8); //multiplex ration mode:63
    write_command_OLED(0x3f);
    write_command_OLED(0xd5); //display divide ratio/osc. freq. mode
    write_command_OLED(0x80);
    write_command_OLED(0x81); //contrast control
    write_command_OLED(0x50);
    write_command_OLED(0xd9); //set pre-charge period
    write_command_OLED(0x21);
    write_command_OLED(0x20); //Set Memory Addressing Mode
    write_command_OLED(0x02);
    write_command_OLED(0xdb); //VCOM deselect level mode
    write_command_OLED(0x30);
    write_command_OLED(0xad); //master configuration
    write_command_OLED(0x00);
    write_command_OLED(0xa4); //out follows RAM content
    write_command_OLED(0xa6); //set normal display
    write_command_OLED(0xaf); // display on 

    write_command_OLED(0x20); // set memory addressing as horisontal
    write_command_OLED(0x00);

    write_command_OLED(0x21);
    write_command_OLED(0x00);
    write_command_OLED(0x7F);//Set start and end address for columns

    write_command_OLED(0x22);
    write_command_OLED(0x00);
    write_command_OLED(0x07); // set  start and end address for pages

    write_command_OLED(0xaf);

}

void OLED_print_start_screen(){
    OLED_goto_line(3);
    OLED_goto_column(10);
    OLED_write_string("CYBERPUNK 2077", 8, 1);
    OLED_goto_line(5);
    OLED_goto_column(10);
    OLED_write_string("Press any button to start...",4,1);

    for(unsigned char i = 1; i < 250; i++){
        // OLED_clear_line(5);
        // OLED_write_string("Press any button to start...",4,1);
        _delay_ms(4);
        write_command_OLED(0x81);
        write_command_OLED(i);
        OLED_goto_line(0);
        OLED_clear_line(0);
        direction_t dir2 = get_direction();
        OLED_write_string(direction_to_string(dir2),8,1);
    } 
    for(unsigned char i = 1; i < 250; i++){
        //OLED_clear_line(5);

        // OLED_write_string("Press any button to start...",4,1);
        _delay_ms(4);
        write_command_OLED(0x81);
        write_command_OLED(250-i);
        OLED_goto_line(0);
        OLED_clear_line(0);
        direction_t dir2 = get_direction();
        OLED_write_string(direction_to_string(dir2),8,1);
    }

}

void write_d(char data){
    //Sett pins høye iht kommando inn, kommandoområde 0x1000-0x11ff
    data_address[0]= data;
}

void OLED_write_char(unsigned char karakter, char size, char invert){
    uint8_t char_data = (uint8_t) karakter - TABLE_OFFSET;
    if(size == 8){
        for(int i = 0; i < 8 ; i++){
        char data_out = (char) pgm_read_byte(&font8[char_data][i]);
        if(!invert){
            data_out = ~(data_out);
        }
        write_d(data_out); 
        } 
    }else if( size == 4){
       for(int i = 0; i < 4 ; i++){
        char data_out = (char) pgm_read_byte(&font4[char_data][i]);
        if(!invert){
            data_out = ~(data_out);
        }
        write_d(data_out); 
        }  
    }else{
        for(int i = 0; i < 5 ; i++){
        char data_out = (char) pgm_read_byte(&font5[char_data][i]);
        if(!invert){
            data_out = ~(data_out);
        }
        write_d(data_out); 
        } 
    }
    
}


void OLED_write_string(char string[], char size, char invert){
    //OLED_clear();
    int i = 0;
    while(string[i] != '\0'){
        OLED_write_char(string[i], size, invert);
        i++;
    }
}

void OLED_goto_line(char line){
    if( (line < 0) || (line > 7)){
        return;
    }
    write_command_OLED(0x22);
    write_command_OLED(line);
    write_command_OLED(0x07);

    OLED_goto_column(0);

}

void OLED_goto_column(char column){
    if((column < 0) ||(column > 127)){
        return;
    }
    write_command_OLED(0x21);
    write_command_OLED(column);
    write_command_OLED(0x7F);
}

void OLED_clear_line(char line){
    if( (line < 0) || (line > 7)){
        return;
    }
    OLED_goto_line(line);
    for(int i = 0; i < SCREEN_W; i++){
        write_d(0x00);
    }
    OLED_goto_line(line);
}

void OLED_home(){
    OLED_goto_line(0);
}

void OLED_pos(char row, char column){
    OLED_goto_line(row);
    OLED_goto_column(column);
}
