#include "includes.h"
#include "joystick_can.h"

void get_and_send_joystick_pos(){
    joystick_positions_t pos = get_joystick_positions();
    can_message msg;
    msg.id = 5;
    msg.data_length = 2;
    msg.data[0] = pos.x;
    msg.data[1] = pos.y;
    CAN_transmit(&msg);
} 
