#ifndef OLED_DRIVER
#define OLED_DRIVER

#include <avr/io.h>

#define TABLE_OFFSET 32
#define SCREEN_W 128 //screen width (pixels)
#define SCREEN_H 8  //screeen height (pages)


void OLED_init(void);

void write_command_OLED(char command);

void OLED_write_char(unsigned char karakter, char size, char invert);

void write_d(char data);

void OLED_clear(void);

void OLED_write_string(char string[], char size, char invert);

void OLED_goto_line(char line);

void OLED_goto_column(char line);

void OLED_clear_line(char line);

void OLED_home();

void OLED_pos(char row, char column);

void OLED_print_start_screen();
#endif