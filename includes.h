#ifndef INCLUDES
#define INCLUDES

#include <avr/io.h>
#include "UART_driver.h"
#include <stdlib.h>
#include <string.h>
#include "misc.h"
#include "adc_driver.h"
#include "OLED_driver.h"
#include "menu.h"
#include "SPI_driver.h"
#include "MCP2515_driver.h"
#include "CAN_driver.h"

#endif