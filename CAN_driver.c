#include "CAN_driver.h"
#include "MCP2515_driver.h"
#include <avr/interrupt.h>
#include "mcp2515.h"
#include "misc.h"

volatile transmit_registers* TXB0;
volatile transmit_registers* RXB0;

#define CNF1 0b00101010
#define CNF2 0b00101001
#define CNF3 0b00101000


ISR(INT0_vect){
    cli();
    printf("vektor kjores\n");
    if(read_mcp_status() & MCP_RX0IF){
        write_mcp(0x00,CANINTF);
        can_message msg;
        CAN_recieve(&msg);
        print_can_message(&msg);
        free(&msg);
    }
    
    sei();
}

void CAN_init(){
    mcp_reset();
    TXB0 = (transmit_registers*) 0b00110000;
    RXB0 = (transmit_registers*) 0b01100000;
    write_mcp(0x03, CNF1); //set bit timing for can
    write_mcp(0b10110001, CNF2); //set bit timing for can
    write_mcp(0x05, CNF3); //set bit timing for can

    cli();
    // enable interrupts on falling edge INT1 atmega
    MCUCR |= (1 << ISC01);

    //enable INT1 globally atmega
    GICR |= (1 << INT0);

    //enable interrupt on rec buffer 0 full in mcp
    write_mcp(0x01,MCP_CANINTE);

    sei();

   // write_mcp(0b01000000, (char) &TXB0->CANCTRL); // SET LOopback mode
    //write_mcp(0b00000000, (char) &TXB0->CANCTRL); // Set normal mode
    write_mcp(0b01100000, (char) &RXB0->CTRL);
    mcp_bit_modify((char) &TXB0->CANCTRL, 0b11100000, 0b00000000);// set normal mode
    //printf("CANSTAT reg: %x \n", read_mcp((char) &TXB0->CANSTAT));
    //printf("CANINTE reg: %x \n", read_mcp(MCP_CANINTE));
}

int CAN_transmit(can_message *m){
    //printf("The value of TXB0->DLC is %p, expected: 00110101", (void*) &TXB0->DLC);
    if(read_mcp((char) &TXB0->CTRL) & (1 << 3)){
        return -1; //check whether txreq==1; a message is waiting for transmission
    }
    if(m->data_length >8){
        return -1;
    }
    write_mcp((uint8_t) (m->id >> 3), (char) &TXB0->SIDH); //write ID to SIDH
    write_mcp((uint8_t) ((m->id & 0x7) <<5) , (char) &TXB0->SIDL);
    write_mcp(m->data_length, (char) &TXB0->DLC); //write data length to DLC
    for (uint8_t i = 0; i < m->data_length; i++){ //write data to buffer
        //printf("Writing %d into address %x \n", m->data[i], 0b00110110+i);
        write_mcp(m->data[i], (char) 0b00110110 +i);

    }
    RTS(0x01);
    return 0;
}

int CAN_recieve(can_message *msg){
    printf("CANINTF : %x \n", read_mcp(CANINTF));
    //kan f.eks. lese CANSTAT for å sjekke at status (CTRL) er satt riktig
    /* if(!(read_mcp(CANINTF) & (0x01))){
        msg->id = 0;
        msg->data_length = 0;
        msg->data[0] = 0;
        return -1;
    }  */
    uint16_t high = read_mcp((char) &RXB0->SIDH);
    uint16_t low = read_mcp((char) &RXB0->SIDL);
/*     printf("ID high: %d   ID low: %d \n", high, low & 0xe0); */
    msg->id = (high << 3) | (low >> 5);
    msg->data_length = ((read_mcp((char) &RXB0->DLC)) & 0x0F)+1;
    for(int i = 0; i < msg->data_length; i++){
        msg->data[i] = read_mcp((char) 0b01100110 +i);

    }
    write_mcp(0x00, CANINTF);
    //mcp_bit_modify(CANINTF, 0x01, 0x00);
    printf("CANINTF : %x \n", read_mcp(CANINTF));
    return 0;


}

ISR(BADISR_vect){}