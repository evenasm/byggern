#ifndef SPI_DRIVER
#define SPI_DRIVER

void SPI_init(void);

char SPI_send_and_rec(char data);

void set_SS_low();

void set_SS_high();

#endif