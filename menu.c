#include "menu.h"
#include <stdlib.h>
#include "OLED_driver.h"
#include <avr/io.h>
#include "UART_driver.h"

struct Node* current_pos; //stores the current position of the "cursor" in the menu
struct Node* current_head; // stores the head of the current sub menu

void init_menu(void){
    struct Node* head = NULL; 
    struct Node* second = NULL; 
    struct Node* third = NULL; 
    struct Node* sub1 = NULL;
    struct Node* sub2 = NULL;
  
    // allocate 3 nodes in the heap 
    head = (struct Node*)malloc(sizeof(struct Node)); 
    second = (struct Node*)malloc(sizeof(struct Node)); 
    third = (struct Node*)malloc(sizeof(struct Node)); 
    sub1 = (struct Node*)malloc(sizeof(struct Node));
    sub2 = (struct Node*)malloc(sizeof(struct Node));

    head->data = "Start game"; // assign data in first node 
    head->next = second; // Link first node with second 
    head->prev = NULL;
    head->sub = sub1;
    head->super = NULL;
    
    sub1->data = "New game";
    sub1->next = sub2;
    sub1->prev = NULL;
    sub1->super = head;
    sub1->sub = NULL;

    sub2->data = "Load save";
    sub2->next = NULL;
    sub2->prev = sub1;
    sub2->super = head;
    sub2->sub = NULL;
  
    second->data = "Einstellungen"; // assign data to second node 
    second->next = third; 
    second->prev = head;
    second->super = NULL;
    second->sub = NULL;
  
    third->data = "Exit"; // assign data to third node 
    third->next = NULL; 
    third->prev = second;
    third->super = NULL;
    third->sub = NULL;



    current_head = head;
    current_pos = head;
}

void print_menu(){
    struct Node* n = current_head;
    OLED_clear();
    int line = 0;
    while(n != NULL){
        OLED_goto_line(line);
        if(current_pos == n){
            OLED_write_string(n->data, 8, 0);
        }else{
            OLED_write_string(n->data, 8, 1);
        }
        n = n->next;
        line++;
    }
    
}

void menu_down(){
    if(current_pos->next != NULL){
        current_pos = current_pos->next;
    }
}

void menu_up(){
    if(current_pos->prev != NULL){
        current_pos = current_pos->prev;
    }
}

struct Node* menu_pos(){
    return current_pos;
}

void menu_sub(){
    if(current_pos->sub != NULL){
        current_pos = current_pos->sub;
        current_head = current_pos;
    }
}

void menu_super(){
    if(current_pos->super != NULL){
        current_pos = current_pos->super;
        current_head = get_head();
    }
}

struct Node* get_head(){
    struct Node *n = current_pos;
    while(n->prev != NULL){
        n = n->prev;
    }
    return n;
}