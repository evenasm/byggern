#include "SPI_driver.h"

char read_mcp(char address){
    set_SS_low();
    SPI_send_and_rec(0x03); //read command
    SPI_send_and_rec(address); //address to be read
    char data = SPI_send_and_rec(0x00); //read the data from given address
    set_SS_high();
    return data;
}

char write_mcp(char data, char address){
    set_SS_low();
    SPI_send_and_rec(0x02);
    SPI_send_and_rec(address);
    SPI_send_and_rec(data);
    set_SS_high();
}

void RTS(unsigned char buffer_number){
    if((buffer_number < 0) || (buffer_number >7)){
        printf("Invalid format");
    }else{
        set_SS_low();
        char command = 0x80 | buffer_number;
        SPI_send_and_rec(command);
        set_SS_high();
    }
    
}

char read_mcp_status(){
    set_SS_low();
    SPI_send_and_rec(0xA0);
    char data = SPI_send_and_rec(0x00);
    set_SS_high();
    return data;
}

void mcp_reset(){
    set_SS_low();
    SPI_send_and_rec(0xC0);
    set_SS_high();
}

void mcp_bit_modify(char address, char mask, char data){
    set_SS_low();
    SPI_send_and_rec(0x05);
    SPI_send_and_rec(address);
    SPI_send_and_rec(mask);
    SPI_send_and_rec(data);
    set_SS_high();
}