#include <avr/io.h>

void SPI_init(void){
    /* Set MOSI, SS and SCK output, all others input */
    DDRB |= (1<<PB5)|(1<<PB7) | (1 << PB4);
    /* Enable SPI, Master, set clock rate fck/16 */
    SPCR = (1<<SPE)|(1<<MSTR)|(1<<SPR0);
}

char SPI_send_and_rec(char data){
    SPDR = data;

    while(!(SPSR & (1<<SPIF)))
        ;
    return SPDR;
}

void set_SS_low(){
    PORTB &= ~(1 << PB4);
}

void set_SS_high(){
    PORTB |= (1 << PB4);
}