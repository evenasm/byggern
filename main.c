
#define F_CPU 4915200
#include <util/delay.h>
#include "includes.h"
#include "joystick_can.h"


int main(void){

    //enable interrupts globally
    SREG |= (1 << 7);

    DDRD &= ~(1 << PD2);

    UART_init(MYUBRR);
    MCUCR |= (1 << SRE);
    SFIOR |= (1 << XMM2);
    ADC_init();
    OLED_init();
    //write_command_OLED(0xaf);
    //OLED_print_start_screen();
    OLED_clear();
    init_menu();
    SPI_init();
    
    


    //volatile char *adc = (char *) 0x1400;
    direction_t dir = get_direction();
    direction_t last_dir = dir;
    char status;
    //mcp_reset();
    CAN_init();
    can_message msg;
    msg.id = 1;
    msg.data_length = 8;
    for (uint8_t i = 0; i <= 7; i++){
        msg.data[i] = 3*i;
    }
    //memcpy((void*) msg.data, (void*) d, msg.data_length);
    /* int flag2 = CAN_transmit(&msg);
        if(flag2 == 0 ){
            printf("Transmit good\n");
        } else{
            printf("Transmit bad\n");
        }
    flag2 = CAN_transmit(&msg);
        if(flag2 == 0 ){
            printf("Transmit good\n");
        } else{
            printf("Transmit bad\n");
        } */

    while (1)
    {
        //printf("CANINTF : %x \n", read_mcp(CANINTF));
        //printf("GIFR: %x \n", GIFR);
        //printf("MCUCR : %x \n", MCUCR);
       //CAN_transmit(&msg);
        //int flag2 = CAN_transmit(&msg);
        //if(flag2 == 0 ){
        //    printf("Transmit good\n");
        //} else{
        //    printf("Transmit bad\n");
        //}
        get_and_send_joystick_pos();
        _delay_ms(100);
        /*
        can_message rec;
        int flag = CAN_recieve(&rec);
        if(flag == 0){
            printf("worked!\n");
            printf("ID =%x\n", rec.id);
            printf("Data length = %d \n", rec.data_length);
            for(int i = 0; i < rec.data_length && (i < 8); i++){
                printf("d[%d] = %d\n", i, (char) rec.data[i]);
            }
            printf("\n"); 
        }
        else{
            printf("Didn't work\n");
        }  */
        /* write_mcp(0x0F, 0x30);
        printf("pre: expected : %x , got: %x \n", 0x0b, read_mcp(0x30));
        mcp_bit_modify(0x30,0xF8, 0x00);
        printf("post: expected : %x , got: %x\n", 0x03, read_mcp(0x30)); */
        /* write_mcp(0x01,0x30);
        if(read_mcp(0x30)){
            OLED_write_string("Dope",8,1);
        }
        _delay_ms(2000);

        write_mcp(0x01,0x30);
       mcp_reset();
        if(read_mcp(0x30)){
            OLED_write_string("nope",8,1);
        }
          */
        
        //write_mcp(0b00001000, 0x30);

        //bitwise_print(status, p);
       // OLED_write_string(p,8,1);
       // printf(p,8,1);
        //OLED_clear();
        //OLED_home();
        /* OLED_clear(); //test for mcp read/write
        OLED_goto_line(0);
        for(char i = 0; i < 25; i++){
           write_mcp((char) 65+i,0x00 );
            char data = read_mcp(0x00);
            OLED_write_char(data,8,1); 
            _delay_ms(500);
        } */
        
        //dir = get_direction();
        //if(dir != last_dir){
        //    if(dir == UP){
        //        menu_up();
        //    }
        //    else if( dir == DOWN){
        //        menu_down();
        //    }else if(dir == RIGHT){
        //        menu_sub();
        //    }else if (dir == LEFT){
        //        menu_super();
        //    }
        //}
        //last_dir = dir;
        //print_menu();
        //OLED_goto_line(4);
        //OLED_write_string(direction_to_string(dir),8,1);
//
        //_delay_ms(1); 
    } 
    
 
}



