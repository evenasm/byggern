#ifndef UART_DRIVER
#define UART_DRIVER

#include <avr/io.h>
#include <stdio.h>

#define FOSC 4915200// Clock Speed
#define BAUD 9600
#define MYUBRR FOSC/16/BAUD-1

/* Initializes the UART.
*/
void UART_init(unsigned int ubrr);

/* Sends and unsigned char on the UART
*/
void UART_transmit(unsigned char data);

/*Receives a char
@return the received char 
*/
unsigned char UART_receive(void);

int put_char(char data, FILE* file);

int get_char(FILE* file);


#endif