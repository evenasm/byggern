#include "adc_driver.h"
#include <stdio.h>

volatile char *adc = (char *) 0x1400; //start of adc

void ADC_init(void){
    DDRB |= 0x01;
    //TCCR0 &= ~(1 <<FOC0);
    //CTC mode
    TCCR0 |= (1 << WGM01);
    //TCCR0 &= ~(1 << WGM00);


    //TCCR0 |= (1 << COM01); 
    TCCR0 |= (1 << COM00);

    TCCR0 |= (1 << CS00);
    //TCCR0 &= ~(1 << CS01);
    //TCCR0 |= (1 << CS02);

    OCR0 = 0; //these lines set up the clock signal at PB0 at half the atmegas clock rate

}

uint8_t x_channel_adc_read(void){
    adc[0] = 0b10000001; // set adc to convert x channel
    _delay_ms(1);
    return adc[0]; // return the now converted value from x channel 
}
uint8_t y_channel_adc_read(void){
    adc[0] = 0b10000000; // set adc to convert x channel
    _delay_ms(1);
    return adc[0]; // return the now converted value from x channel 
}

joystick_positions_t get_joystick_positions(void){
    joystick_positions_t pos;
    uint8_t x =  x_channel_adc_read();
    uint8_t y =  y_channel_adc_read();
    pos.x = x;
    pos.y = y;
    return pos;
}

direction_t get_direction(void){
    joystick_positions_t pos = get_joystick_positions();
    int x = (int) pos.x -33;
    int y = (int) pos.y-33;
    if((x > 115) && ( x <141) && (y < 141) && (y > 115)){
        return NEUTRAL;
    }
    if((y >= x) && (y >= (255-x))){
        return UP;
    }
    if((y <= x) && (y >= (255-x))){
        return RIGHT;
    }
    if((y >= x) && (y <=(255-x))){
        return LEFT;
    }
    if((y <=x) && (y <= (255-x))){
        return DOWN;
    }
    return ERROR;
}

/*Reads left slider through adc
*/
uint8_t read_left_slider(void){
    adc[0] = 0b10000010;
    _delay_ms(1);
    return adc[0];
}

uint8_t read_right_slider(void){
     adc[0] = 0b10000011;
    _delay_ms(1);
    return adc[0];
}

char* direction_to_string(direction_t dir){
    switch (dir)
    {
    case 0:
        return "LEFT";
        break;
    case 1:
        return "RIGHT";
        break;
    case 2:
        return "UP";
        break;
    case 3:
        return "DOWN";
        break;
    case 4:
        return "NEUTRAL";
        break;
    default:
        return "ERROR";
        break;
    }
}
