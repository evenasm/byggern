#ifndef CAN_driver
#define CAN_driver
#include <stdint.h>

#define CANINTF 0x2c
//Registerdef
typedef struct{
    uint8_t CTRL;
    uint8_t SIDH;
    uint8_t SIDL;
    uint8_t EID8;
    uint8_t EID0;
    uint8_t DLC;
    uint8_t D0;
    uint8_t D1;
    uint8_t D2;
    uint8_t D3;
    uint8_t D4;
    uint8_t D5;
    uint8_t D6;
    uint8_t D7;
    uint8_t CANSTAT;
    uint8_t CANCTRL;
}transmit_registers;

//Meldingsdef
typedef struct{
uint16_t id;
uint8_t data_length;
uint8_t data[8];

}can_message;

void CAN_init();

int CAN_transmit(can_message *m);

int CAN_recieve(can_message *msg); //returns -1 when no new message recieved

#endif